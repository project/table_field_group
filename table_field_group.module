<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * @file
 * Extends Field Group module with an option to make a 'Table Columns' group for
 * viewing mode.
 * Children fields will be displayed in per columns look.
 */

/**
 * Implements hook_help().
 */
function table_field_group_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.table_field_group':
      $readme = __DIR__ . '/README.md';
      $text = file_get_contents($readme);
      $output = '';

      // If the Markdown module is installed, use it to render the README.
      if ($text && \Drupal::moduleHandler()
          ->moduleExists('markdown') === TRUE) {
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        $output = $filter->process($text, 'en');
      }
      // Else the Markdown module is not installed output the README as text.
      else {
        if ($text) {
          $output = '<pre>' . $text . '</pre>';
        }
      }

      // Add a link to the Drupal.org project.
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Field Group Table with fields in columns project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/table_field_group',
      ]);
      $output .= '</p>';

      return $output;
  }
}
